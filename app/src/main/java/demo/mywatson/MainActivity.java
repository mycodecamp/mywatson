package demo.mywatson;

import android.content.res.ColorStateList;
import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ibm.watson.developer_cloud.assistant.v1.Assistant;
import com.ibm.watson.developer_cloud.assistant.v1.model.InputData;
import com.ibm.watson.developer_cloud.assistant.v1.model.MessageOptions;
import com.ibm.watson.developer_cloud.assistant.v1.model.MessageResponse;
import com.ibm.watson.developer_cloud.http.HttpMediaType;
import com.ibm.watson.developer_cloud.service.security.IamOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.SpeechToText;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechRecognitionAlternative;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechRecognitionResult;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechRecognitionResults;
import com.ibm.watson.developer_cloud.speech_to_text.v1.websocket.BaseRecognizeCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import omrecorder.AudioChunk;
import omrecorder.AudioRecordConfig;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.PullableSource;
import omrecorder.Recorder;

public class MainActivity extends AppCompatActivity {


    private boolean listening = false;
    private Recorder recorder;
    private Assistant assistant;
    private SpeechToText speechService;

    private List<Message> messages;
    private RecyclerView messageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        this.messages = new ArrayList<>();

        {
            this.messageList = findViewById(R.id.list_message);
            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            this.messageList.setLayoutManager(llm);
            this.messageList.setAdapter(new MessagesAdapter(messages));
        }


        this.assistant = new Assistant("2018-08-01", new IamOptions.Builder()
                .apiKey("in9pGGA1EpgmNg8-uB-uuX8NmNmHrAW8XvCdxn-5TeIE")
                .build());
        this.assistant.setApiKey("in9pGGA1EpgmNg8-uB-uuX8NmNmHrAW8XvCdxn-5TeIE");
        this.assistant.setEndPoint("https://gateway-wdc.watsonplatform.net/assistant/api");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                    public MessageResponse response_msg;

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        Toast.makeText(MainActivity.this, "response: " + response_msg, Toast.LENGTH_LONG).show();

                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            InputData input = new InputData.Builder("tubarao").build();
                            MessageOptions options = new MessageOptions.Builder("086dce2a-0f34-4827-95dc-ac675c0d1cb0")
                                    .input(input)
                                    .build();
                            this.response_msg = assistant.message(options).execute();
                        } catch (Exception e) {
                            Log.i("mywatson", e.getLocalizedMessage(), e);
                        }


                        return null;
                    }
                };
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            }
        });


        this.speechService = new SpeechToText(new IamOptions.Builder()
                .apiKey("uwIEK7vFCcSPglWfP5KfphnKr4Ja3Dvg9HuSUjdwhUfG")
                .build());
        this.speechService.setApiKey("uwIEK7vFCcSPglWfP5KfphnKr4Ja3Dvg9HuSUjdwhUfG");
        this.speechService.setEndPoint("https://gateway-wdc.watsonplatform.net/speech-to-text/api");


        final FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab_mic);
        fab1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    fab1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));
                    recorder = OmRecorder.wav(
                            new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                                @Override
                                public void onAudioChunkPulled(AudioChunk audioChunk) {
                                    // animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                                }
                            }), file());
                    recorder.startRecording();
                    Toast.makeText(MainActivity.this, "Fale no Mic", Toast.LENGTH_SHORT).show();

                    return true;
                }

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    fab1.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));

                    final String path = file().getAbsolutePath();

                    AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                        public SpeechRecognitionResults transcript;

                        @Override
                        protected Void doInBackground(Void... voids) {
                            try {
                                recorder.stopRecording();

                                File audio = new File(path);

                                RecognizeOptions options = new RecognizeOptions.Builder()
                                        .audio(audio)
                                        .model("pt-BR_BroadbandModel")
                                        .contentType(HttpMediaType.AUDIO_WAV)
                                        .build();

                                this.transcript = speechService.recognize(options).execute();

                            } catch (IllegalStateException e1) {
                                recorder = OmRecorder.wav(
                                        new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                                            @Override
                                            public void onAudioChunkPulled(AudioChunk audioChunk) {
                                                // animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                                            }
                                        }), file());
                            } catch (Exception e) {
                                Log.i("mywatson", e.getLocalizedMessage(), e);

                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            Toast.makeText(getBaseContext(), this.transcript + "", Toast.LENGTH_SHORT).show();
                            if (this.transcript != null) {

                                List<SpeechRecognitionResult> results = this.transcript.getResults();
                                for (SpeechRecognitionResult result : results) {
                                    List<SpeechRecognitionAlternative> alternatives = result.getAlternatives();
                                    for (SpeechRecognitionAlternative alternative : alternatives) {
                                        final Message msg = new Message();
                                        msg.message = alternative.getTranscript();
                                        msg.type = 1;
                                        messages.add(msg);

                                        {
                                            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

                                                public MessageResponse response_msg;

                                                @Override
                                                protected void onPostExecute(Void aVoid) {
                                                    Toast.makeText(MainActivity.this, "response: " + response_msg, Toast.LENGTH_SHORT).show();


                                                    if (response_msg != null) {
                                                        List<String> texts = response_msg.getOutput().getText();
                                                        for (String text : texts) {
                                                            final Message msg = new Message();
                                                            msg.message = text;
                                                            msg.type = 0;
                                                            messages.add(msg);
                                                        }
                                                        messageList.getAdapter().notifyDataSetChanged();
                                                    }
                                                }

                                                @Override
                                                protected Void doInBackground(Void... voids) {
                                                    try {
                                                        InputData input = new InputData.Builder(msg.message).build();
                                                        MessageOptions options = new MessageOptions.Builder("086dce2a-0f34-4827-95dc-ac675c0d1cb0")
                                                                .input(input)
                                                                .build();
                                                        this.response_msg = assistant.message(options).execute();
                                                    } catch (Exception e) {
                                                        Log.i("mywatson", e.getLocalizedMessage(), e);
                                                    }


                                                    return null;
                                                }
                                            };
                                            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                        }
                                    }
                                }

                                messageList.getAdapter().notifyDataSetChanged();
                            }
                        }

                    };

                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    return true;
                }

                return false;
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private PullableSource mic() {
        return new PullableSource.Default(
                new AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 44100
                )
        );
    }

    private File file() {
        return new File(Environment.getExternalStorageDirectory(), "kailashdabhi.wav");
    }


    public class MessagesAdapter extends RecyclerView.Adapter {
        private List<Message> messages;

        public MessagesAdapter(List<Message> messages) {
            this.messages = messages;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView1 = LayoutInflater.from(MainActivity.this)
                    .inflate(R.layout.item_layout, parent, false);
            return new MessageHolder(itemView1);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            Message msg = messages.get(position);

            if (msg.type == 1) {
                ((MessageHolder) holder).lbl.setText(msg.message);
                ((MessageHolder) holder).lbl.setTextColor(getResources().getColor(R.color.colorAccent));
                ((MessageHolder) holder).img.setImageResource(android.R.drawable.ic_dialog_email);
                ((MessageHolder) holder).img.setColorFilter(getResources().getColor(R.color.colorAccent));
            }
            if (msg.type == 0) {
                ((MessageHolder) holder).lbl.setText(msg.message);
                ((MessageHolder) holder).lbl.setTextColor(getResources().getColor(R.color.colorPrimary));
                ((MessageHolder) holder).img.setImageResource(android.R.drawable.ic_popup_reminder);
                ((MessageHolder) holder).img.setColorFilter(getResources().getColor(R.color.colorPrimary));
            }
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

    }

    public class MessageHolder extends RecyclerView.ViewHolder {

        protected TextView lbl;
        protected ImageView img;

        public MessageHolder(View itemView) {
            super(itemView);

            lbl = itemView.findViewById(R.id.lbl1);
            img = itemView.findViewById(R.id.img);
        }


    }

    public class Message {
        public String message;
        public int type;
    }
}
